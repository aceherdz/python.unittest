import unittest
from com.binarios import binarios

class Test_binarios(unittest.TestCase):
    binario = ''
    def setUp(self):
        self.binario = binarios()

    def test_caso_correcto(self):        
        self.assertEqual(self.binario.to_binario(2),'10')

    def test_caso_0(self):
        self.assertEqual(self.binario.to_binario(0),0)

    def test_invertir(self):
        binario = self.binario
        self.assertEqual(binario.invertir(["1","0","0"]),'001')


if __name__ == '__main__':
    unittest.main()