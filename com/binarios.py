class binarios:

    def invertir(self,arreglo):
        invertido = ""
        for i in range(len(arreglo)-1, -1, -1):
            invertido = invertido + str(arreglo[i])
        return invertido


    def to_binario(self,numero):
        resultado = []
        if numero == 0:
            return 0
        while numero > 1:
            resultado.append(numero % 2)
            numero = numero // 2
        resultado.append(numero)
        return self.invertir(resultado)


