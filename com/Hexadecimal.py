class Hexadecimal:


    def to_hex(self,numero):
        resultado = []
        if numero == 0:
            return 0
        while numero > 1:
            resultado.append(numero % 16)
            numero = numero // 16
        if numero > 0:
            resultado.append(numero)

        return self.invertir(resultado)


    def poner_letricas(self,numero):
        if numero < 10:
            return str(numero)
        return {"10": "A", "11": "B", "12": "C", "13": "D", "14": "E", "15": "F"}[str(numero)]


    def invertir(self,arreglo):
        invertido = ""
        for i in range(len(arreglo) - 1, -1, -1):
            invertido = invertido + self.poner_letricas(arreglo[i])
        return invertido


